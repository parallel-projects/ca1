#include "stdio.h"
#include "x86intrin.h"

typedef union {
    __m128i             int128;
    
    unsigned char        m128_u8[16];
    signed char            m128_i8[16];

    unsigned short        m128_u16[8];
    signed short        m128_i16[8];
    
    unsigned int        m128_u32[4];
    signed int        m128_i32[4];
    
    unsigned long int        m128_u64[2];
    signed long int        m128_i64[2];
    
    unsigned short        m128_u128[1];
    signed short        m128_i128[1];
} intVec;

typedef union {
    __m128      sfloat128;
    float       m128_f32[4];
} floatVec;

void print_u8 (__m128i a)
{
    intVec tmp;
    tmp.int128 = a;
    printf ("[");
    for (int i=15; i>0; i--) {
        printf ("%X, ", tmp.m128_u8[i]);
    }
    printf ("%X]\n\n", tmp.m128_u8[0]);
}

void print_i8 (__m128i a)
{
    intVec tmp;
    tmp.int128 = a;
    printf ("[");
    for (int i=15; i>0; i--) {
        printf ("%d, ", tmp.m128_i8[i]);
    }
    printf ("%d]\n\n", tmp.m128_i8[0]);
}

void print_u16 (__m128i a)
{
    intVec tmp;
    tmp.int128 = a;
    printf ("[");
    for (int i=7; i>0; i--) {
        printf ("%X, ", tmp.m128_u16[i]);
    }
    printf ("%X]\n\n", tmp.m128_u16[0]);
}

void print_i16 (__m128i a)
{
    intVec tmp;
    tmp.int128 = a;
    printf ("[");
    for (int i=7; i>0; i--) {
        printf ("%d, ", tmp.m128_i16[i]);
    }
    printf ("%d]\n\n", tmp.m128_i16[0]);
}

void print_u32 (__m128i a)
{
    intVec tmp;
    tmp.int128 = a;
    printf ("[");
    for (int i=3; i>0; i--) {
        printf ("%X, ", tmp.m128_u32[i]);
    }
    printf ("%X]\n\n", tmp.m128_u32[0]);
}

void print_i32 (__m128i a)
{
    intVec tmp;
    tmp.int128 = a;
    printf ("[");
    for (int i=3; i>0; i--) {
        printf ("%d, ", tmp.m128_i32[i]);
    }
    printf ("%d]\n\n", tmp.m128_i32[0]);
}

void print_u64 (__m128i a)
{
    intVec tmp;
    tmp.int128 = a;
    printf ("[");
    for (int i=1; i>0; i--) {
        printf ("%lX, ", tmp.m128_u64[i]);
    }
    printf ("%lX]\n\n", tmp.m128_u64[0]);
}

void print_i64 (__m128i a)
{
    intVec tmp;
    tmp.int128 = a;
    printf ("[");
    for (int i=1; i>0; i--) {
        printf ("%ld, ", tmp.m128_i64[i]);
    }
    printf ("%ld]\n\n", tmp.m128_i64[0]);
}

void print_spfp(__m128 a)
{
    floatVec tmp;
    tmp.sfloat128 = a;
    printf ("[");
    for (int i=3; i>0; i--) {
        printf ("%f, ", tmp.m128_f32[i]);
    }
    printf ("%f", tmp.m128_f32[0]);
    printf("]\n\n");
}

void print_int_vector(__m128i a, unsigned char type)
{
    switch(type)
    {
        case '1':   // u8
            print_u8(a);
            break;
        case '2':   // i8
            print_i8(a);
            break;
        case '3':   // u16
            print_u16(a);
            break;
        case '4':   // i16
            print_i16(a);
            break;
        case '5':   // u32
            print_u32(a);
            break;
        case '6':   // i32
            print_i32(a);
            break;
        case '7':   // u64
            print_u64(a);
            break;
        case '8':   // i64
            print_i64(a);
            break;
        default:
            printf("%s\n", "Invalid type!");
    }
}

void print_spfp_vector (__m128 a) {
    print_spfp(a);
}

unsigned char intArray [16] = {    0X00, 0X11, 0X22, 0X33, 0X44, 0X55, 0X66, 0X77,
                                0X88, 0X99, 0XAA, 0XBB, 0XCC, 0XDD, 0XEE, 0XFF};

int main(void)
{
    __m128i a;
    a = _mm_load_si128((const __m128i*)intArray);

    print_int_vector(a, '1');
    print_int_vector(a, '2');
    print_int_vector(a, '3');
    print_int_vector(a, '4');
    print_int_vector(a, '5');
    print_int_vector(a, '6');
    print_int_vector(a, '7');
    print_int_vector(a, '8');

    __m128 b;
    b = _mm_load_ps((const float*)intArray);
    print_spfp_vector(b);

    return 0;
}