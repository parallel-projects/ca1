CC=g++

CFLAGS=-c
INCDIR=-I.

PROGRAMS := task1 task2

all: $(PROGRAMS)

main: task1.cpp task2.cpp
	$(CC) -o  $@ $^
	
clean:
	rm -f *.o $(PROGRAMS)
